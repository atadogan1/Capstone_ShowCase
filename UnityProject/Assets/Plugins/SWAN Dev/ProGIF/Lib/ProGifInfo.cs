﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProGifInfo : ProGifPlayerComponent 
{
	public void GetInfo(string loadPath, Action<FirstGifFrame> onComplete)
	{
		onComplete +=(firstFrame)=>{
			Clear();
			GameObject.Destroy(this.gameObject);
		};
		SetOnFirstFrameCallback(onComplete);
		LoadGifFromUrl(loadPath);
		this.loadPath = loadPath;
	}

	public override void Play(int fps, Sprite[] sprites){}

	protected override void _OnFrameReady(GifTexture gTex, bool isFirstFrame){}

	protected override void _AddGifTexture (GifTexture gTex)
	{
		Debug.Log("Called _AddGifTexture() in ProGifInfo");
	}

//	public override void Clear()
//	{
//		base.Clear();
//	}

}
