﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GUITexture))]
public sealed class ProGifPlayerGuiTexture : ProGifPlayerComponent
{
	[HideInInspector] public GUITexture destinationGuiTexture;				// The GUITexture for display textures
	private List<GUITexture> m_ExtraGuiTextures = new List<GUITexture>();

	void Awake()
	{
		if(destinationGuiTexture == null)
		{
			destinationGuiTexture = gameObject.GetComponent<GUITexture>();
		}
	}

	// Update gif frame for the Player (Update is called once per frame)
	void Update()
	{
		if(State == PlayerState.Playing && displayType == ProGifPlayerComponent.DisplayType.GuiTexture)
		{
			if(Time.time >= nextFrameTime)
			{
				spriteIndex = (spriteIndex >= gifTextures.Count - 1)? 0 : spriteIndex + 1;
				nextFrameTime = Time.time + interval;
			}

			if(spriteIndex < gifTextures.Count)
			{
				if(OnPlayingCallback != null) OnPlayingCallback(gifTextures[spriteIndex]);

				if(destinationGuiTexture != null)
				{
					destinationGuiTexture.texture = gifTextures[spriteIndex].GetTexture2D();
				}

				if(m_ExtraGuiTextures != null && m_ExtraGuiTextures.Count > 0)
				{
					Texture2D tex = gifTextures[spriteIndex].GetTexture2D();
					for(int i = 0; i < m_ExtraGuiTextures.Count; i++)
					{
						if(m_ExtraGuiTextures[i] != null)
						{
							m_ExtraGuiTextures[i].texture = tex;
						}
						else
						{
							m_ExtraGuiTextures.Remove(m_ExtraGuiTextures[i]);
							m_ExtraGuiTextures.TrimExcess();
						}
					}
				}
			}
		}
	}

	public override void Play(int fps, Sprite[] sprites)
	{
		base.Play(fps, sprites);

		if(destinationGuiTexture == null)
		{
			destinationGuiTexture = gameObject.GetComponent<GUITexture>();
		}
		if(destinationGuiTexture != null)
		{
			destinationGuiTexture.texture = gifTextures[0].GetTexture2D();
		}
	}

	protected override void _OnFrameReady(GifTexture gTex, bool isFirstFrame)
	{
		if(isFirstFrame)
		{
			if(destinationGuiTexture != null) 
			{
				destinationGuiTexture.texture = gifTextures[0].GetTexture2D();
			}
		}
	}

	public override void Clear()
	{
		//Your Code before base.Clear():
		if(destinationGuiTexture != null && destinationGuiTexture.texture != null)
		{
			Texture2D.Destroy(destinationGuiTexture.texture);
		}

		base.Clear();
	}


	public void ChangeDestination(GUITexture guiTexture)
	{
		destinationGuiTexture = guiTexture;
	}

	public void AddExtraDestination(GUITexture guiTexture)
	{
		if(!m_ExtraGuiTextures.Contains(guiTexture))
		{
			m_ExtraGuiTextures.Add(guiTexture);
		}
	}

	public void RemoveFromExtraDestination(GUITexture guiTexture)
	{
		if(m_ExtraGuiTextures.Contains(guiTexture))
		{
			m_ExtraGuiTextures.Remove(guiTexture);
			m_ExtraGuiTextures.TrimExcess();
		}
	}
}
