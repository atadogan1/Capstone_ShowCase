﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Renderer))]
public sealed class ProGifPlayerRenderer : ProGifPlayerComponent
{
	[HideInInspector] public Renderer destinationRenderer;				// The renderer for display textures
	private List<Renderer> m_ExtraRenderers = new List<Renderer>();

	void Awake()
	{
		if(destinationRenderer == null)
		{
			destinationRenderer = gameObject.GetComponent<Renderer>();
		}
	}

	// Update gif frame for the Player (Update is called once per frame)
	void Update()
	{
		if(State == PlayerState.Playing && displayType == ProGifPlayerComponent.DisplayType.Renderer)
		{
			if(Time.time >= nextFrameTime)
			{
				spriteIndex = (spriteIndex >= gifTextures.Count - 1)? 0 : spriteIndex + 1;
				nextFrameTime = Time.time + interval;
			}

			if(spriteIndex < gifTextures.Count)
			{
				if(OnPlayingCallback != null) OnPlayingCallback(gifTextures[spriteIndex]);

				if(destinationRenderer != null) 
				{
					destinationRenderer.material.mainTexture = gifTextures[spriteIndex].GetTexture2D();
				}

				if(m_ExtraRenderers != null && m_ExtraRenderers.Count > 0)
				{
					Texture2D tex = gifTextures[spriteIndex].GetTexture2D();
					for(int i = 0; i < m_ExtraRenderers.Count; i++)
					{
						if(m_ExtraRenderers[i] != null)
						{
							m_ExtraRenderers[i].material.mainTexture = tex;
						}
						else
						{
							m_ExtraRenderers.Remove(m_ExtraRenderers[i]);
							m_ExtraRenderers.TrimExcess();
						}
					}
				}
			}
		}
	}

	public override void Play(int fps, Sprite[] sprites)
	{
		base.Play(fps, sprites);

		if(destinationRenderer == null)
		{
			destinationRenderer = gameObject.GetComponent<Renderer>();
		}
		if(destinationRenderer != null && destinationRenderer.material != null)
		{
			destinationRenderer.material.mainTexture = gifTextures[0].GetTexture2D();
		}
	}

	protected override void _OnFrameReady(GifTexture gTex, bool isFirstFrame)
	{
		if(isFirstFrame)
		{
			if(destinationRenderer != null && destinationRenderer.material != null) 
			{
				destinationRenderer.material.mainTexture = gifTextures[0].GetTexture2D();
			}
		}
	}

	public override void Clear()
	{
		//Your Code before base.Clear():
		if(destinationRenderer != null && destinationRenderer.material != null && destinationRenderer.material.mainTexture != null)
		{
			Texture2D.Destroy(destinationRenderer.material.mainTexture);
		}

		base.Clear();
	}


	public void ChangeDestination(Renderer renderer)
	{
		destinationRenderer = renderer;
	}

	public void AddExtraDestination(Renderer renderer)
	{
		if(!m_ExtraRenderers.Contains(renderer))
		{
			m_ExtraRenderers.Add(renderer);
		}
	}

	public void RemoveFromExtraDestination(Renderer renderer)
	{
		if(m_ExtraRenderers.Contains(renderer))
		{
			m_ExtraRenderers.Remove(renderer);
			m_ExtraRenderers.TrimExcess();
		}
	}
}
