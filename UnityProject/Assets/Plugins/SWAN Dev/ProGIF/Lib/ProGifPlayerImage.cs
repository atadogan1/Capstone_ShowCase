﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public sealed class ProGifPlayerImage : ProGifPlayerComponent
{
	[HideInInspector] public Image destinationImage;						// The image for display sprites
	private List<Image> m_ExtraImages = new List<Image>();

	void Awake()
	{
		if(destinationImage == null)
		{
			destinationImage = gameObject.GetComponent<Image>();
		}
	}

	// Update gif frame for the Player (Update is called once per frame)
	void Update()
	{
		if(State == PlayerState.Playing && displayType == ProGifPlayerComponent.DisplayType.Image)
		{
			if(Time.time >= nextFrameTime)
			{
				spriteIndex = (spriteIndex >= gifTextures.Count - 1)? 0 : spriteIndex + 1;
				nextFrameTime = Time.time + interval;
			}

			if(spriteIndex < gifTextures.Count)
			{
				if(OnPlayingCallback != null) OnPlayingCallback(gifTextures[spriteIndex]);

				if(destinationImage != null)
				{
					destinationImage.sprite = gifTextures[spriteIndex].GetSprite();
				}

				if(m_ExtraImages != null && m_ExtraImages.Count > 0)
				{
					Sprite sp = gifTextures[spriteIndex].GetSprite();
					for(int i = 0; i < m_ExtraImages.Count; i++)
					{
						if(m_ExtraImages[i] != null)
						{
							m_ExtraImages[i].sprite = sp;
						}
						else
						{
							m_ExtraImages.Remove(m_ExtraImages[i]);
							m_ExtraImages.TrimExcess();
						}
					}
				}
			}
		}
	}

	public override void Play(int fps, Sprite[] sprites)
	{
		base.Play(fps, sprites);

		if(destinationImage == null)
		{
			destinationImage = gameObject.GetComponent<Image>();
		}
		if(destinationImage != null)
		{
			destinationImage.sprite = gifTextures[0].GetSprite();
		}
	}

	protected override void _OnFrameReady(GifTexture gTex, bool isFirstFrame)
	{
		if(isFirstFrame)
		{
			if(destinationImage != null) 
			{
				destinationImage.sprite = gifTextures[0].GetSprite();
			}
		}
	}

	public override void Clear()
	{
		//Your Code before base.Clear():
		if(destinationImage != null && destinationImage.sprite != null && destinationImage.sprite.texture != null)
		{
			Texture2D.Destroy(destinationImage.sprite.texture);
		}

		base.Clear();
	}


	public void ChangeDestination(Image image)
	{
		destinationImage = image;
	}

	public void AddExtraDestination(Image image)
	{
		if(!m_ExtraImages.Contains(image))
		{
			m_ExtraImages.Add(image);
		}
	}

	public void RemoveFromExtraDestination(Image image)
	{
		if(m_ExtraImages.Contains(image))
		{
			m_ExtraImages.Remove(image);
			m_ExtraImages.TrimExcess();
		}
	}
}
