﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1SelectButton : MonoBehaviour {

    [SerializeField]
    KeyCode key = KeyCode.F1;
    [SerializeField]
    UnityEngine.Events.UnityEvent @event;


	void Update ()
    {
        if (Input.GetKeyDown(key))
            @event.Invoke();
	}
}
