﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimationEvents : MonoBehaviour {

    private Animator animator;

    [SerializeField]
    private LogoDrop logoDrop;

    public bool Idle
    {
        get
        {
            return animator.GetBool("Idle");
        }
        set
        {
            animator.SetBool("Idle", value);
        }
    }

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void FaceSmash()
    {
        CameraShake.Control.PunchCamera();
        logoDrop.Drop = true;
    }

    public void FaceShake()
    {

    }

    public void BlastOffIn()
    {

    }

    public void BlastOffOut()
    {

    }

    public void Wink()
    {
        
    }

    public void SlideIn()
    {

    }

    public void ResetIdleValue()
    {
        Idle = true;
    }
}
