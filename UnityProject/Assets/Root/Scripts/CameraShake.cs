using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{

	
	// How long the object should shake for.
	static public float shakeDuration = 0f;
	
	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	
	Vector3 originalPos;
	
    public static CameraShake Control;
	
	
	void OnEnable()
	{
        Control = this;
		originalPos = transform.localPosition;
	}

	void Update()
	{
		if (shakeDuration > 0)
		{
			transform.localPosition = new Vector3(originalPos.x, transform.localPosition.y, originalPos.z) + Random.insideUnitSphere * shakeAmount;
			
			shakeDuration -= Time.deltaTime * decreaseFactor;
		}
		else
		{
			shakeDuration = 0f;
			transform.localPosition = new Vector3 (originalPos.x, transform.localPosition.y, originalPos.z);
		}
	}

    private GameObject GetObject()
    {
        return this.gameObject;
    }

    public void PunchCamera()
    {
        iTween.PunchPosition(gameObject, Vector3.right * 5, 1f);
    }
}
