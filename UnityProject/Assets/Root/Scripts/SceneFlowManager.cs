﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class SceneFlowManager : MonoBehaviour {

    //Enter names in Click Next or Enter //Text enter and Button
    //Show Instructions and make them Click Ready! //Text and Button
    //3.2.1... start recording //Text
    //Gif record & save //Done //Connect Recorder UI
    //Uploading.... //Text
    // IF (Failed) show failed Screen then > Reload Scene? restart recording? //Text, Button?
    //Go Check Out your picture on Twitter @DakaDaka.games //Text //Done Button or 5 Second wait
    //Reload Scene
    [SerializeField]
    private GameObject backgroundPanel;

    [Header("Name Entry UI Elements")]
    [SerializeField]
    private GameObject nameEntryUI;
    [SerializeField]
    private InputField nameEntry;
    //[SerializeField]
    //private Button nameEntryNextBut;

    [Header("Instructions UI Elements")]
    [SerializeField]
    private GameObject instructionsUI;
    //[SerializeField]
    //private Text instructionsText;
    //[SerializeField]
    //private Button intructionsReady;

    [Header("Countdown UI Elements")]
    [SerializeField]
    private GameObject countDownUI;
    [SerializeField]
    private Text countDownText;

    [Header("Recording Saving Sharing UI Elements")]
    [SerializeField]
    private GameObject recordingSavingSharingUI;
    [SerializeField]
    private Text savingText;
    [SerializeField]
    private Text uploadingText;

    [Header("Failed Screen UI Elements")]
    [SerializeField]
    private GameObject failedUI;

    [Header("Success Screen UI Elements")]
    [SerializeField]
    private GameObject successUI;
    [SerializeField]
    private Text checkItOutText;


    private void OnEnable()
    {
        AtaProGifMgr.OnSavingStart += SavingGif;
        TwitterShare.OnTweetUpload += UploadingToTwitterUI;
        TwitterShare.OnTweetCallback += TwitterCallBackReceiver;
    }

    private void OnDisable()
    {
        AtaProGifMgr.OnSavingStart -= SavingGif;
        TwitterShare.OnTweetUpload -= UploadingToTwitterUI;
        TwitterShare.OnTweetCallback -= TwitterCallBackReceiver;
    }

    private void Start()
    {
        //make sure everything but nameentry is active
        nameEntryUI.SetActive(true);
        backgroundPanel.SetActive(true);
        instructionsUI.SetActive(false);
        countDownUI.SetActive(false);
        recordingSavingSharingUI.SetActive(false);
        failedUI.SetActive(false);
        successUI.SetActive(false);
        uploadingText.gameObject.SetActive(false);
        savingText.gameObject.SetActive(false);
    }


    public static event System.Action<string> OnNameEntryDone;

    //Put on NExt Button
    public void NameEntryDone()
    {
        //Put name entry into twitterShare
        OnNameEntryDone(nameEntry.text);
        //set name entry false
        nameEntryUI.SetActive(false);
        //set instruction true
        instructionsUI.SetActive(true);

    }

    //Button On Ready Button
    public void InstructionsReady()
    {
        //disable instructions
        instructionsUI.SetActive(false);
        //activate countdown
        StartCoroutine(RecordingCountDown());
    }

    private int countDownLength = 3;
    IEnumerator RecordingCountDown()
    {
        backgroundPanel.SetActive(false);
        countDownUI.SetActive(true);

        countDownText.text = "Ready!";
        yield return new WaitForSeconds(1);
        countDownText.text = "Set!";

        AnimationManager.Control.QueueCharacters();

        yield return new WaitForSeconds(1);
        countDownText.text = "Action!";
        
        //start recording 1 second before
        AtaProGifMgr.Instance.Record();

        yield return new WaitForSeconds(1);

        //Set countdown UI false
        countDownUI.SetActive(false);
        //Set recording UI true
        recordingSavingSharingUI.SetActive(true);
        //Start Recording
        //AtaProGifMgr.Instance.Record();
    }



    public void SavingGif()
    {
        //OnSavingStart();
        //savingText.gameObject.SetActive(false);
        //backgroundPanel.SetActive(true);
        StartCoroutine(WaitingForSave());
    }

    bool savingDone;
    IEnumerator WaitingForSave()
    {
        backgroundPanel.SetActive(true);

        savingText.text = "Saving";
        savingText.gameObject.SetActive(true);

        while (savingDone == false)
        {
            //animate until callback from twitter Share received 
            for (int i = 0; i < 4; i++)
            {
                savingText.text += ".";
                yield return new WaitForSeconds(0.1f);
            }
            for (int i = 0; i < 4; i++)
            {
                savingText.text = savingText.text.Remove(savingText.text.Length - 1);
                yield return new WaitForSeconds(0.1f);
            }
            yield return null;
        }


    }


    private bool callBackReceived;
    private bool callBackSuccess;
    IEnumerator WaitingForUpload()
    {
        savingText.gameObject.SetActive(false);
        savingDone = true;
        uploadingText.text = "Uploading";
        uploadingText.gameObject.SetActive(true);

        while (callBackReceived == false)
        {
            //animate until callback from twitter Share received 
            for (int i = 0; i < 4; i++)
            {
                uploadingText.text += ".";
                yield return new WaitForSeconds(0.1f);
            }
            for (int i = 0; i < 4; i++)
            {
                uploadingText.text = uploadingText.text.Remove(uploadingText.text.Length - 1);
                yield return new WaitForSeconds(0.1f);
            }
            yield return null;
        }

        //turn ui off
        recordingSavingSharingUI.SetActive(false);

        //if(failed){open failed}else open success
        if (!callBackSuccess)
        {
            FailedUpload();
        }
        else
        {
            SuccesfullyUploadedUI();
        }

        backgroundPanel.SetActive(true);
    }

    //Listening to twitter 
    void UploadingToTwitterUI()
    {
        StartCoroutine(WaitingForUpload());
    }

    void TwitterCallBackReceiver(bool success)
    {
        callBackReceived = true;
        callBackSuccess = success;
    }

    void FailedUpload()
    {
        failedUI.SetActive(true);
    }


    void SuccesfullyUploadedUI()
    {

        MakeSuccessMessage(TwitterShare.processedTwitterNames);
        //turn on UI
        successUI.SetActive(true);

    }

    //take in text from TwitterShare
    void MakeSuccessMessage(string names)
    {
        checkItOutText.text = "<b>"+ names + "</b>\n" + "Go check out your picture on Twitter\n" + "<b>@DakaDakaGames</b>";
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    




}
