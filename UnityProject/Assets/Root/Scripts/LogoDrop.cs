﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoDrop : MonoBehaviour {

    private Animator animator;

    public bool Drop
    {
        get
        {
            return animator.GetBool("Drop");
        }
        set
        {
            animator.SetBool("Drop", value);
        }
    }

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void ResetDropValue()
    {
        Drop = false;
    }
}
