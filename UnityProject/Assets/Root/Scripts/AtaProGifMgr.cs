﻿/// <summary>
/// Created by SWAN DEV
/// </summary>

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class AtaProGifMgr : MonoBehaviour
{

    public static AtaProGifMgr _instance;
    public static AtaProGifMgr Instance
    {
        get
        {
            return _instance;
        }
    }

    void Start()
    {
        _instance = this;
    }


    public Slider sld_Progress;
    public Text text_Progress;

    public void UpdateRecordOrSaveProgress(float progress)
    {
        if (sld_Progress != null) sld_Progress.value = progress;
        if (text_Progress != null) text_Progress.text = "Progress: " + (int)(100 * progress) + " %";

    }

    public void SetGifProgressColor(Color color)
    {
        if (sld_Progress.fillRect.GetComponent<Image>().color != color)
        {
            sld_Progress.fillRect.GetComponent<Image>().color = color;
        }
    }


    public Action _OnStartRecord = null;
    public Action<float> _OnRecordProgress = null;
    public Action _OnRecordDurationMax = null;

    public void Setup(Action onStartRecord = null, Action<float> onRecordProgress = null, Action onRecordDurationMax = null)
    {
        _OnStartRecord = onStartRecord;
        _OnRecordProgress = onRecordProgress;
        _OnRecordDurationMax = onRecordDurationMax;
    }

    public void Record()
    {
        Setup(() => {
            //Update UI
            SetGifProgressColor(ProGifManager.GetColor(ProGifManager.CommonColorEnum.LightYellow));

        }, UpdateRecordOrSaveProgress, () => {
            Debug.Log("DemoMgr - Record duration MAX.");
            StopRecord();

        });

        ProGifManager.Instance.StartRecord(Camera.main, _OnRecordProgress, _OnRecordDurationMax);

        if (_OnStartRecord != null)
        {
            _OnStartRecord();
        }

    }

    public void PauseRecord()
    {
        Debug.Log("Pause Recording");
        ProGifManager.Instance.PauseRecord();
    }

    public void ResumeRecord()
    {
        Debug.Log("Resume Recording");
        ProGifManager.Instance.ResumeRecord();
    }

    public static event System.Action OnSavingStart;

    public void StopRecord()
    {
        Debug.Log("Start making GIF");
        OnSavingStart();
        //SceneFlowManager.OnSavingStart();
        ProGifManager.Instance.StopAndSaveRecord(
            () => {
                Debug.Log("On recorder pre-processing done.");
            },

            (id, progress) => {
                UpdateRecordOrSaveProgress(progress);
                SetGifProgressColor(ProGifManager.GetColor(ProGifManager.CommonColorEnum.Red));
            },

            (id, path) => {
                //If a specify aspect ratio is provided, then we should play the gif using its path: Set loadFile = true
                bool loadFile = ProGifManager.Instance.LoadFile;

                UpdateRecordOrSaveProgress(1f);
                StartCoroutine(_OnFileSaved());
            }
        );

    }

    private IEnumerator _OnFileSaved()
    {
        yield return new WaitForSeconds(2f);
        _ResetGifProgress();
    }

    private void _ResetGifProgress()
    {
        UpdateRecordOrSaveProgress(0f);
        SetGifProgressColor(ProGifManager.GetColor(ProGifManager.CommonColorEnum.White));
    }

    private IEnumerator _OnLoadingComplete()
    {
        yield return new WaitForSeconds(2f);
        _ResetGifProgress();
    }


    public void SetButtonState(Button button, Color color, bool enable)
    {
        button.enabled = enable;
        button.image.color = color;
    }


}
