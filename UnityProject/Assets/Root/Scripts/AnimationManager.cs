﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour {

    public static AnimationManager Control;

    [SerializeField]
    private AnimationEvents[] animEvents;

    public void Awake()
    {
        Control = this;
    }

    public void QueueCharacters(float delay1 = 0, float delay2 = 0)
    {
        StartCoroutine(Delay1(delay1));
        StartCoroutine(Delay2(delay2));
    }

    private IEnumerator Delay1(float time)
    {
        yield return new WaitForSeconds(time);
        animEvents[0].Idle = false;
    }

    private IEnumerator Delay2(float time)
    {
        yield return new WaitForSeconds(time);
        animEvents[1].Idle = false;
    }

}
