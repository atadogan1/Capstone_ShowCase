﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Twity.DataModels.Core;
using System.IO;
using System.Text.RegularExpressions;
using Sirenix.OdinInspector;

public class TwitterShare : MonoBehaviour {

    //public string consumerKey, consumerSecret, accessToken, accessTokenSecret;

    [SerializeField]
    private TwitterCredentials[] creditials;
    private static int index;


    [SerializeField]
    private string twitterNamesToTweetAt;

    static public string processedTwitterNames;

    public string defaultNoText, defaultSingularText, defaultPluralText;

    private const string twitterRegexMatch = "(?<=^|(?<=[^a-zA-Z0-9-\\.]))@([A-Za-z0-9_]+)";

    void Start()
    {

        Twity.Oauth.consumerKey = creditials[index].consumerKey;
        Twity.Oauth.consumerSecret = creditials[index].consumerSecret;
        Twity.Oauth.accessToken = creditials[index].accessToken;
        Twity.Oauth.accessTokenSecret = creditials[index].accessTokenSecret;

        ++index;
        index %= creditials.Length;
    }

    private void OnEnable()
    {

        ProGifManager.OnSavePathLocation += GetSavePath;
        SceneFlowManager.OnNameEntryDone += SetTwitterNames;
    }
    private void OnDisable()
    {
        ProGifManager.OnSavePathLocation -= GetSavePath;
        SceneFlowManager.OnNameEntryDone += SetTwitterNames;
    }

    public static event System.Action OnTweetUpload;
    private void GetSavePath(string savePath)
    {
        //print("ran" + savePath);
        //;
        OnTweetUpload();
        UploadTweet(savePath, MakeStatusUpdateText());

    }

    private void SetTwitterNames(string names)
    {
        twitterNamesToTweetAt = names;
    }
    
    [Button("Test Status Update Text")]
    string MakeStatusUpdateText()
    {
       // string statusUpdateText = "";

        //What if person had no TWitter Account
        //take out of string[] names 
        //put in seperate area
        //how to know if twitter account or not?

        List<string> validNames = new List<string>();

        string[] names = twitterNamesToTweetAt.Split(',', ';',' ');

        for (int i = 0; i < names.Length; i++)
        {

            names[i] = "@" + names[i];
        }

        for (int i = 0; i < names.Length; i++)
        {
            if (Regex.IsMatch(names[i], twitterRegexMatch))
            {
                Match validName = Regex.Match(names[i], twitterRegexMatch);
                // print(validName);
                validNames.Add(validName.Value);
            }
        }

        string namesCombined = "";

        for (int i = 0; i < validNames.Count; i++)
        {
            if (validNames.Count > 1)
            {
                //if this is name before last name then put in and
                if (i == validNames.Count - 2)
                {
                    namesCombined += validNames[i] + ", and ";
                }
                //if last name no coma
                else if (i == validNames.Count - 1)
                {
                    namesCombined += validNames[i] + " ";
                }
                else
                {
                    namesCombined += validNames[i] + ", ";
                }
            }
            else
            {
                namesCombined += validNames[i] + ' ';
            }
            
        }


        //This is so that sceneflow gets the combined twitter names
        processedTwitterNames = namesCombined;

        string statusUpdateText;
        if(validNames.Count == 0)
        {
            statusUpdateText = defaultNoText;
        }
        else if (validNames.Count > 1)
        {
            //use plural
            statusUpdateText = namesCombined + defaultPluralText;
        }
        else
        {
            //use singular
            statusUpdateText = namesCombined + defaultSingularText;
        }


        print(statusUpdateText);

        return statusUpdateText;
            //names[i] = names[i].Remove('@');
            //names[i] = names[i].Remove(' ');

            //names[i] = "@" + names[i] + " ";

            // if names[i].length > 2 (3, 4, 5...) // is a real account?
        //}


        //statusUpdateText = defaultText;
    }

    private string statusText;
    void UploadTweet(string path, string status)
    {
        statusText = status;

        byte[] imgBinary = File.ReadAllBytes(path);
        string imgbase64 = System.Convert.ToBase64String(imgBinary);

        Dictionary<string, string> parameters = new Dictionary<string, string>();
        parameters["media_data"] = imgbase64;
        //parameters["additional_owners"] = "additional owner if you have";
        StartCoroutine(Twity.Client.Post("media/upload", parameters, MediaUploadCallback));
    }

    void MediaUploadCallback(bool success, string response)
    {
        if (success)
        {
            UploadMedia media = JsonUtility.FromJson<UploadMedia>(response);

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters["media_ids"] = media.media_id.ToString();
            parameters["status"] = statusText;
            StartCoroutine(Twity.Client.Post("statuses/update", parameters, StatusesUpdateCallback));
        }
        else
        {
            Debug.Log(response);
            OnTweetCallback(success);
        }
    }

    public static event System.Action<bool> OnTweetCallback;
    void StatusesUpdateCallback(bool success, string response)
    {
        if (success)
        {
            Tweet tweet = JsonUtility.FromJson<Tweet>(response);
            
        }
        else
        {
            Debug.Log(response);
        }
        OnTweetCallback(success);
    }

}
