﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TwitterCredentials : ScriptableObject
{
    public string consumerKey;
    public string consumerSecret;
    public string accessToken;
    public string accessTokenSecret;

}
