﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableActivateInputField : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        GetComponentInChildren<UnityEngine.UI.InputField>().Select();
	}

}
